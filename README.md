# Rounds #

### Installation Guide ###

Rounds is built using React & React Router. In order to run this project, `git clone` this repo, then `cd rounds` and `npm install` or `yarn` to install the dependencies. To start the server run `npm start` and visit `localhost:3000` (if it doesn't open automatically)

### API ###

In order to fulfil the vision of this magical project. I require the following API:

In Rounds we sort bars by location closest to the user, currently this is done in the client, but it would be much better if this was handled by the API, like so:
`/bars?lat=foo&long=bar`
This would return all the bars sorted by closeness to the user, I would expect an JSON array of the following structure back
```
{
 id,
 name,
 lat,
 long,
 imageURL,
 distance // Int null if lat & long is not defined (meters)
}
```

The second endpoint I would require would be for individual bars, `/bar/id` this would return the following structure:
```
{
 name, // Only make use of name at the moment but the whole bar object could be passed back
 products: [
  {
    id,
    name,
    imageURL,
    currentPrice
  }
 ]
}
```
Lastly I would need two POST endpoints `/rounds` and `/orderedBeverages` to store each round in our B/e