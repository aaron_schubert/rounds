import React from 'react';
import ReactDOM from 'react-dom';
import Rounds from './Rounds';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Rounds />, div);
});
