import React from 'react';
import ReactDOM from 'react-dom';
import Rounds from './Rounds';
import './index.css';
import { Router, Route, browserHistory, IndexRoute } from 'react-router';
import { Bars, Bar } from './Components';

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={Rounds}>
      <IndexRoute component={Bars}/>
      <Route path="/bar/:barId" component={Bar} />
    </Route>
  </Router>,
  document.getElementById('root')
);
