import geolib from 'geolib';
export default function calculateDistance (from, to) {
  if (!from.lat || !from.long) return null;
  if (!to.lat || !to.long) return null;
  return geolib.getDistance(
    {latitude: from.lat, longitude: from.long},
    {latitude: to.lat, longitude: to.long}
  );
};
