export default function getCurrentLocation () {
  return {
    lat: 55.8613,
    long: -4.2564
  };
}
