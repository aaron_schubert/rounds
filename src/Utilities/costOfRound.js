export default function costOfRound (round) {
  if (!round) return;
  let totalCost = 0;
  Object.keys(round.drinks).forEach(id => {
    const drink = round.drinks[id];
    const total = drink.amount * drink.currentPrice;
    totalCost += total;
  })
  return totalCost;
}
