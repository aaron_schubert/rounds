import React, { Component } from 'react';
import './Rounds.css';
import { Link } from 'react-router';

class Rounds extends Component {
  render() {
    return (
      <div className="app">
        <Link
          className="app__header"
          to="/"
        >
          <h1>
            {'Rounds ™'}
          </h1>
        </Link>
        <div className="app__content">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Rounds;
