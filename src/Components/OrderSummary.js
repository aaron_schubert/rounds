import React from 'react';
import { costOfRound } from '../Utilities';

const OrderSummary = ({ round }) => {
  return (
    <div className="orderSummary">
      <p>{`Round ${round.id}`}</p>
      {Object.keys(round.drinks).map(id => {
        const drink = round.drinks[id];
        // Double equals used on purpose here
        // eslint-disable-next-line
        if (drink.amount == 0) return null;
        const total = drink.amount * drink.currentPrice;
        return (
          <p key={drink.id}>
            {`${drink.amount}x ${drink.name} = £${total.toFixed(2)}`}
          </p>
        )
      })}
      <p>{`Cost: £${costOfRound(round).toFixed(2)}`}</p>
    </div>
  )
};

export default OrderSummary;
