import React, { Component } from 'react';
import RowItem from './RowItem';
import { Link } from 'react-router';
import { getBars } from '../API';
import './Bars.css';

export default class Bars extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bars: []
    };
  }
  componentDidMount () {
    getBars().then((bars) =>
      this.setState({
        bars
      })
    );
  }
  render () {
    const { bars } = this.state;
    return (
      <div>
        <h2>{'Bars'}</h2>
        {bars.map(bar => {
          const userIsHere = bar.distance != null && bar.distance <= 5;
          const subtitle = bar.distance != null && (userIsHere ? 'YOU ARE HERE' : `${bar.distance}m away`);
          return (
            <Link
              key={bar.id}
              className="link"
              to={{
                pathname:`/bar/${bar.id}`,
                query: { name: bar.name }
              }}
            >
              <RowItem
                highlighted={userIsHere}
                imageURL={bar.imageURL}
                title={bar.name}
                subtitle={subtitle}
              />
            </Link>
          )
        })}
      </div>
    )
  }
};
