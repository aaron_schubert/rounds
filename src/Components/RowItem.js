import React from 'react';
import './RowItem.css';
const RowItem = ({ imageURL, title, subtitle, highlighted = false, children }) => {
  return (
    <div className={`item ${highlighted && 'item--highlighted'}`}>
      {imageURL && (
        <img
          className="item__image"
          role="presentation"
          src={imageURL}
        />
      )}
      <div className="item__detail">
        <h3 className="item__detail_title">{title}</h3>
        {subtitle != null && (
          <p className={`item__detail_subtitle ${highlighted && 'item__detail_subtitle--highlighted'}`}>
            {subtitle}
          </p>
        )}
        {children}
      </div>
    </div>
  )
};

export default RowItem;
