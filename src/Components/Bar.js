import React, { Component } from 'react';
import RowItem from './RowItem';
import { getProducts, postRound } from '../API';
import OrderSummary from './OrderSummary';
import './Bar.css';
import { costOfRound } from '../Utilities';

export default class Bar extends Component {
  constructor (props) {
    super(props);
    this.state = {
      products: [],
      rounds: [],
      orderedBeverages: []
    };
  }

  componentDidMount () {
    const { params } = this.props;
    getProducts(params.barId).then(products =>
      this.setState({
        products
      }, this.startNewRound)
    );
  }

  startNewRound = () => {
    const { rounds, currentRound } = this.state;
    if (currentRound) {
      currentRound.orderedAt = new Date();
      postRound(currentRound);
    }
    const round = {
      id: rounds.length+1,
      barId: parseInt(this.props.params.barId, 10),
      drinks: {}
    };
    this.setState({
      currentRound: round,
      rounds: [...rounds, round]
    });
  }



  onQuantityChange (product, value) {
    if (value >= 0) {
      const { currentRound } = this.state;
      currentRound.drinks[product.id] = {
        ...product,
        amount: Math.round(value)
      };
      this.setState({ currentRound });
    }
  }

  render () {
    const { query } = this.props.location;
    const { products, currentRound } = this.state;
    return (
      <div>
        <h3>{query.name}</h3>
        <p>
          {"You can order any number of these drinks. Just select the amount of drinks you want and a summary that you can take to the bar will be displayed here."}
        </p>
        { currentRound && (
          <div>
            <OrderSummary
              round={currentRound}
            />
            {costOfRound(currentRound) > 0 && (
              <button
                onClick={this.startNewRound}
              >
                {'New Round'}
              </button>
            )}
          </div>
        )}
        {products.map(product => {
          return (
            <RowItem
              key={product.id}
              imageURL={product.imageURL}
              title={product.name}
              subtitle={product.currentPrice.toFixed(2)}
            >
              <input
                key={`${product.id}` + (currentRound ? currentRound.id :  '')}
                min="0"
                className="amount"
                type="number"
                placeholder="Quantity"
                onChange={(e) => this.onQuantityChange(product, e.target.value)}
              />
            </RowItem>
          )
        })}
      </div>
    )
  }
}
