export default function postRound (round) {
  const { drinks, ...data } = round;
  sendRoundToServer(data);
  Object.keys(drinks).forEach(id => {
    const drink = drinks[id];
    const beverage = {
      roundId: round.id,
      productId: drink.id,
      actualPrice: drink.amount * drink.currentPrice
    };
    sendOrderedBeveragesToServer(beverage);
  });
}

function sendRoundToServer (round) {
  console.log('Sending round to server', round);
}
function sendOrderedBeveragesToServer (beverage) {
  // Increment id on server
  // Server cut's actualPrice down to 4 decimal places (if needed)
  console.log('Sending beverage to server', beverage);
}
