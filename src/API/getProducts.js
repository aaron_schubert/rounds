const products = {
  1 : [
    {
      'id' : 1,
      'name' : 'Angry Sailor',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_381688183271637.jpg',
      'currentPrice' : 6.50
    },
    {
      'id' : 2,
      'name' : 'Orange Mule',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_520224658431052.jpg',
      'currentPrice' : 12.75
    },
    {
      'id' : 4,
      'name' : 'The Manhattan',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_626124316164679.jpg',
      'currentPrice' : 9.99
    },
    {
      'id' : 5,
      'name' : 'The New Old Fashioned',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_434622843076959.jpg',
      'currentPrice' : 3.50
    },
  ],
  2 : [
    {
      'id' : 1,
      'name' : 'Angry Sailor',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_381688183271637.jpg',
      'currentPrice' : 6.50
    },
    {
      'id' : 2,
      'name' : 'Orange Mule',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_520224658431052.jpg',
      'currentPrice' : 12.75
    },
    {
      'id' : 4,
      'name' : 'The Manhattan',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_626124316164679.jpg',
      'currentPrice' : 9.99
    },
    {
      'id' : 5,
      'name' : 'The New Old Fashioned',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_434622843076959.jpg',
      'currentPrice' : 3.50
    },
    {
      'id' : 6,
      'name' : 'Mason / Dixon ',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_941613472828762.jpg',
      'currentPrice' : 17.50
    }
  ],
  3 : [
    {
      'id' : 1,
      'name' : 'Angry Sailor',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_381688183271637.jpg',
      'currentPrice' : 6.50
    },
    {
      'id' : 2,
      'name' : 'Orange Mule',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_520224658431052.jpg',
      'currentPrice' : 12.75
    },
    {
      'id' : 3,
      'name' : 'The FanDuel Special',
      'imageURL' : 'http://www.underconsideration.com/brandnew/archives/fanduel_monogram_detail.png',
      'currentPrice' : 2.25
    },
    {
      'id' : 4,
      'name' : 'The Manhattan',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_626124316164679.jpg',
      'currentPrice' : 9.99
    },
    {
      'id' : 5,
      'name' : 'The New Old Fashioned',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_434622843076959.jpg',
      'currentPrice' : 3.50
    },
  ],
  4 : [
    {
      'id' : 1,
      'name' : 'Angry Sailor',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_381688183271637.jpg',
      'currentPrice' : 6.50
    },
    {
      'id' : 2,
      'name' : 'Orange Mule',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_520224658431052.jpg',
      'currentPrice' : 12.75
    },
    {
      'id' : 4,
      'name' : 'The Manhattan',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_626124316164679.jpg',
      'currentPrice' : 9.99
    },
    {
      'id' : 5,
      'name' : 'The New Old Fashioned',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_434622843076959.jpg',
      'currentPrice' : 3.50
    },
  ],
  5 : [
    {
      'id' : 1,
      'name' : 'Angry Sailor',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_381688183271637.jpg',
      'currentPrice' : 6.50
    },
    {
      'id' : 2,
      'name' : 'Orange Mule',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_520224658431052.jpg',
      'currentPrice' : 12.75
    },
    {
      'id' : 4,
      'name' : 'The Manhattan',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_626124316164679.jpg',
      'currentPrice' : 9.99
    },
    {
      'id' : 5,
      'name' : 'The New Old Fashioned',
      'imageURL' : 'http://www.barlouie.com/files/pagemult/335_502_434622843076959.jpg',
      'currentPrice' : 3.50
    },
  ]
};

export default function getProducts (barId) {
  return Promise.resolve(products[barId]);
}
