import { calculateDistance, getCurrentLocation } from '../Utilities';

export default function getBars () {
  return fetchBars()
        .then(assignDistances)
        .then(sortBarsByDistance);
}

function assignDistances (bars) {
  const currentLocation = getCurrentLocation();
  return Promise.resolve(bars.map(bar => {
    bar.distance = calculateDistance(currentLocation, bar);
    return bar;
  }));
}

function sortBarsByDistance (bars) {
  return Promise.resolve(
    bars.sort((a, b) => {
      if (a.distance === null) return 1;
      if (b.distance === null) return -1;
      if (a.distance < b.distance) return -1;
      if (a.distance > b.distance) return 1;
      return 0;
    })
  );
}

function fetchBars () {
  return Promise.resolve([
    {
      id: 1,
      name: 'Iron Horse'
    },
    {
      id: 2,
      name: 'The Horseshoe Bar',
      lat: '55.8608',
      long: '-4.2563',
      imageURL: 'http://img01.beerintheevening.com/5a/5a9d97e2ff9b9f3b18190dcf4569aea6.jpg'
    },
    {
      id: 3,
      name: 'The Drum & Monkey',
      lat: '55.8613',
      long: '-4.2564',
      imageURL: 'https://media-cdn.tripadvisor.com/media/photo-s/03/d7/43/bc/drum-monkey.jpg'
    },
    {
      id: 4,
      name: 'Missoula',
      lat: '55.8624',
      long: '-4.2558',
      imageURL: 'https://pbs.twimg.com/profile_images/3650467266/81c381e5e0bca5a50c75d99bbd4017d9.jpeg'
    },
    {
      id: 5,
      name: 'The Maltman',
      lat: '55.8628',
      long: '-4.2564',
      imageURL: 'https://media-cdn.tripadvisor.com/media/photo-s/09/4e/e3/67/the-maltman.jpg'
    }
  ]);
}
